'use strict';
(function () {
    angular.module('myApp.search', ['ngRoute'])

        .config(['$routeProvider', function($routeProvider) {
			$routeProvider.when('/search/:title', {
                templateUrl: 'js/search/search.view.html',
                controller: 'searchCrl'
            });
        }]);
})();