'use strict';
(function () {
    angular.module('myApp.search')
        .controller('searchCrl', ['$scope', '$routeParams', '$http', '$location', function($scope, $routeParams, $http, $location) {
			
			$scope.titel = $routeParams.title;
			$scope.min = 0;
			$scope.max = 1000000;
			$scope.sort = "Подешевле";
			var flag = true;
			
		    $http({method: 'GET', url: '/item?title=' + $routeParams.title, }).success(function(data){
			    $scope.items = data;
			    console.log(data);
			});		
			
		    $scope.priceRange = function(item) {
				return item.price >= $scope.min && item.price <= $scope.max;
		    };
			
			$scope.reset = function() {
				$scope.min = 0;
			    $scope.max = 1000000;
				$scope.query = "";
			}
			
			$scope.go = function(query) {
                $location.path('/search/' + query);
			}
						
			$scope.change = function() {
				
				if(flag === true){
					$scope.items.sort(asc);
					$scope.sort = "Подороже";
					flag = false
				} else {
					$scope.items.sort(desc);
					$scope.sort = "Подешевле";
					flag = true
				}
				
				function asc(a, b) {
				  return a.price - b.price;
				}
				
				function desc(a, b) {
				  return b.price - a.price;
				}

			}
        }]);
})();